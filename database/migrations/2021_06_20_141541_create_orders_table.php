<?php

use Carbon\Carbon;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

/**
 * CreateOrdersTable Class to create orders table if not exists
 * 
 * @package Orders
 * @author Gaurav Kumar Sharma <gaurav.kumar07@nagarro.com>
 * @version 1.0.0
 * @access public
 */
class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('origin_lat', 100);
            $table->string('origin_long', 100);
            $table->string('dest_lat', 100);
            $table->string('dest_long', 100);
            $table->integer('distance', false, true)->length(10);
            $table->string('status', 20);
            $table->integer('created_at', false, true)->default(Carbon::now()->timestamp);
            $table->integer('updated_at', false, true)->default(Carbon::now()->timestamp);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
