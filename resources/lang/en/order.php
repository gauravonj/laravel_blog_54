<?php

return [
    'invalid_origin_lat_long' => 'invalid origin lat long values',
    'invalid_destination_lat_long' => 'invalid destination lat long values',
    'order_api_response_success' => 'api success',
    'order_api_response_error' => 'api error',
    'update_success' => 'order updated successfully',
    'update_failure' => 'order failed to update',
    'invalid_order_id' => 'order id must be non-zero',
];
