<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'v1',
], function () {

    Route::get('/', function (Request $request) {
        return [
            'status' => 'ok',
            'message' => 'api is working'
        ];
    })->name('api.index');

    Route::get('orders', '\App\Http\Controllers\OrderController@index')->name('orders.index');
    Route::post('orders', '\App\Http\Controllers\OrderController@store')->name('orders.store');
    Route::patch('orders/{id}', '\App\Http\Controllers\OrderController@update')->name('orders.patch');

   /*  Route::fallback(function (Request $request) {
        return response()->json(['status' => 'not ok', 'message' => 'invalid api endpoint'], 200);
    }); */
});


/* Route::fallback(function () {
    return response()->json(['status' => 'not ok', 'message' => 'invalid api endpoint'], 200);
}); */