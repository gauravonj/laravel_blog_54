<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    
    protected $table = 'orders';

    const UNASSIGNED = 'UNASSIGNED';
    const TAKEN = 'TAKEN';

    protected $fillable = [
        'origin_lat',
        'origin_long',
        'dest_lat',
        'dest_long',
        'distance',
        'status'
    ];

    protected $hidden = [
        'origin_lat',
        'origin_long',
        'dest_lat',
        'dest_long',
        'updated_at',
        'created_at',
    ];

    /**
     * getDateFormat function
     *
     * @return string
     */
    public function getDateFormat()
    {
        return 'U';
    }
}
