<?php

namespace App\Http\Controllers;

use App\Order;
use Illuminate\Http\Request;
use App\Services\OrderService;
use App\Http\Requests\ListOrderRequest;
use App\Http\Requests\PatchOrderRequest;
use App\Http\Requests\StoreOrderRequest;
use Illuminate\Http\Response;

class OrderController extends BaseApiController
{
    protected $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(ListOrderRequest $request)
    {
        try {
            $orders = $this->orderService->all($request->all());
            if (!empty($orders) ) {
                return $this->apiSuccessResponse($orders);
            } else {
                return $this->apiSuccessResponse([]);
            }
        } catch (\Exception $ex) {
            return $this->apiExceptionResponse($ex);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreOrderRequest $request)
    {
        $originLat  = $request->origin[0];
        $originLong = $request->origin[1];

        $destLat  = $request->destination[0];
        $destLong = $request->destination[1];

        try {
            $orderData = [
                'origin_lat'        => $originLat,
                'origin_long'       => $originLong,
                'dest_lat'          => $destLat,
                'dest_long'         => $destLong
            ];
            $order =  $this->orderService->createOrder($orderData);

            if ( !empty($order) ) {
                return $this->apiSuccessResponse($order, Response::HTTP_OK );
            }
        } catch (\Exception $ex) {
            return $this->apiExceptionResponse($ex);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\PatchOrderRequest $request
     * @param  integer  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PatchOrderRequest $request, $id)
    {
        try {
            $order = $this->orderService->update($id, $request->all());
            if ($order) {
                return $this->apiSuccessResponse(['status' => trans('order.update_success')], Response::HTTP_OK );
            } else {
                return $this->apiErrorResponse(trans('order.update_failure'), Response::HTTP_NOT_FOUND );
            }
        } catch (\Exception $ex) {
            return $this->apiExceptionResponse($ex);
        }

    }

}
