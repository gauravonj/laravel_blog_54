<?php

namespace App\Http\Controllers;
use Illuminate\Http\Response;

class BaseApiController extends Controller
{
    private $responseStatus = [
        'data' => [
            'error' => true,
            'status_code' => Response::HTTP_BAD_REQUEST,
            'message' => '',
            'data' => [],
        ]
    ];

    public function apiSuccessResponse($data = [], $headerStatusCode = Response::HTTP_OK, $headers = [])
    {
        return response()->json($data, $headerStatusCode);
    }


    public function apiErrorResponse($error, $headerStatusCode = Response::HTTP_UNPROCESSABLE_ENTITY, $headers = [])
    {
        return response()->json(['error' => is_array($error) ? $error->first() : $error], $headerStatusCode);
    }


    public function apiExceptionResponse($exception)
    {
        return response()->json(['error' => $exception->getMessage()], Response::HTTP_UNPROCESSABLE_ENTITY);
    }

}
