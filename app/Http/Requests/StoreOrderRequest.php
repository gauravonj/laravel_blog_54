<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Http\Response as HttpResponse;

class StoreOrderRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'origin' => 'required|array|size:2|orderlatlong',
            'destination' => 'required|array|size:2|orderlatlong'
        ];
    }

    /**
     * messages function to get the custom validation messages
     *
     * @return array
     */
    public function messages()
    {
        return [
            'origin.orderlatlong' => trans('order.invalid_origin_lat_long'),
            'destination.orderlatlong' => trans('order.invalid_destination_lat_long')
        ];
    }

    /**
     * failedValidation function
     *
     * @param \Illuminate\Contracts\Validation\Validator $validator
     *
     * @return Illuminate\Http\Exceptions\HttpResponseException
     */
    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['error' => $validator->errors()->first()], HttpResponse::HTTP_UNPROCESSABLE_ENTITY));
    }
}
