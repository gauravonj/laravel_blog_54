<?php
namespace App\Repositories;

/**
 * OrderRepositoryInterface interface.
 */
interface OrderRepositoryInterface
{
    public function all($data = array());
    public function create(array $data);
    public function createOrder(array $data);
    public function update($id, array $data);
    public function delete($id);
    public function show($id);
}