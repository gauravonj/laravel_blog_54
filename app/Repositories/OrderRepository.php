<?php
namespace App\Repositories;

use App\Repositories\OrderRepositoryInterface;
use App\Services\Curl;
use Illuminate\Database\Eloquent\Model;
use App\Order;

class OrderRepository implements OrderRepositoryInterface
{
    protected $model;

    protected $curl;

    public function __construct(Model $model, Curl $curl)
    {
        $this->model = $model;
        $this->curl = $curl;
    }

    public function all($data = array())
    {
        return $this->model->all();
    }

    public function create(array $data)
    {
        return $this->model->create($data);
    }

    public function update($id, array $data)
    {
        $order = $this->model->find($id);
        if ($order) {
            $response = $order->update($data);
            return $response;
        }

        throw new \Exception("requested order id not found, please check the order");
        
    }

    public function delete($id)
    {
        return $this->model->destroy($id);
    }

    public function show($id)
    {
        return $this->model->findOrFail($id);
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($model)
    {
        $this->model = $model;
        return $this;
    }

    public function with($relations)
    {
        return $this->model->with($relations);
    }

    public function createOrder(array $data)
    {
        //$this->curl->get()
        $data['distance'] = 100;
        $data['status'] = Order::UNASSIGNED;
        return $this->create($data);
    }
    
}
