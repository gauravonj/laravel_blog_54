<?php
namespace App\Services;


use App\Repositories\OrderRepositoryInterface;

class OrderService
{
    protected $orderRepository;

    public function __construct(OrderRepositoryInterface $orderRepository)
    {
        $this->orderRepository = $orderRepository;
    }

    public function update($id, $data)
    {
        return $this->orderRepository->update($id, $data);
    }

    public function createOrder(array $orderData)
    {
        return $this->orderRepository->createOrder($orderData);
    }

    public function all($data)
    {
        return $this->orderRepository->all($data);
    }

}
