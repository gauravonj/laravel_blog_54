<?php

namespace App\Services;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Log;

/**
 * Curl class is used to perform http request
 *
 * @version       1.1
 * @package       Blog
 * @category      Blog
 * @author        Gaurav Kumar Sharma <gaurav.kumar07@nagarro.com>
 */
class Curl
{
    protected $client = null;
    protected $responseBody = [];

    /**
     * Constructor Curl class constructor
     *
     * @author Nagarro
     * @category Blog
     * @package Blog
     *
     * @param \GuzzleHttp\Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
        $this->responseBody = [
            'status' => Response::HTTP_OK,
            'message' => null,
            'data' => [],
        ];
    }

    /**
     * get function to send a GET http request
     *
     * @param string $url
     * @param array $params
     * @param array $headers
     *
     * @return mixed
     */
    public function get($url = null, $params = [], $headers = [])
    {
        $response = null;
        $finalResponseData = [];
        try {
            $request = $this->client->request('GET', $url, [
                'query' => $params,
            ]);

            $response = $request->getBody();
        } catch (ClientException $exception) {
            Log::error("Curl Exception :: " . json_encode($exception));
            throw new \Exception($exception->getMessage());
        }

        if ($response != null) {
            $finalResponseData = json_decode($response, true);
        } else {
            $finalResponseData = $response;
        }

        return $this->prepareResponse($finalResponseData);
    }

    /**
     * prepareResponse function
     *
     * @param mixed $data
     * @param integer $status
     * @param array $headers
     *
     * @return array
     */
    public function prepareResponse($data = null, $status = Response::HTTP_OK, $headers = [])
    {
        $responseData = null;
        if (is_string($data) && $this->isJson($data)) {
            $responseData = json_decode($data);
        } else {
            $responseData = $data;
        }

        $this->responseBody['status'] = $status;
        $this->responseBody['message'] = trans('order');
        $this->responseBody['data'] = $responseData;

        return response()->json($this->responseBody, $status, $headers);

    }

    /**
     * Check if string is json or not
     *
     * @param string $string
     *
     * @return boolean
     */
    private function isJson($string)
    {
        json_decode($string);
        return json_last_error() === JSON_ERROR_NONE;
    }

}
